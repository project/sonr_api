
-- SUMMARY --

This module saves conntections to multiple sOnr instances and offers the
interface and additional helper functions to work with them.

-- REQUIREMENTS --

- A s0nr-server has to be existent (The configuration of the server can be done
via child-modules).
- PPT & PPX (PoolParty Enterprise Edition) are required if filtering by
concepts/free terms is required.
- cURL needs to be installed on the web server your Drupal-instance runs on.
- the chr-module is required for all the communication between sOnr and Drupal.


-- INSTALLATION --

Install the "chr"-module (https://drupal.org/project/chr) and the
"sOnr API"-module (https://drupal.org/project/sonr_api) as usual.
see https://drupal.org/documentation/install/modules-themes/modules-7 for
further information.

-- USAGE --

- Activate first the chr-module and then the sOnr API-module
- Configure the module at admin/config/services/sonr-api.
- Add one of the existing child-modules to use the functionality of this module
or create your own one. Current child-modules are:
  - Content Aggregator (includes Content Aggregator Trends)
