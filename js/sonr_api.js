(function ($) {
Drupal.behaviors.sonr_api = {
  attach: function (context) {

    // check if the individual sOnr configuration is available
    $("div.sonr-led").each(function() {
      var $this = $(this);
      var url = Drupal.settings.basePath + "admin/config/services/sonr-api/" + $this.attr('data-id') + "/available";
      $.get(url, function (data) {
        if (data == 1) {
          var led = 'led-green';
          var title = 'Service available';
        }
        else {
          var led = 'led-red';
          var title = 'Service NOT available';
        }
        $this.addClass(led);
        $this.attr('title', title);
      });
    });

  }
};
})(jQuery);
