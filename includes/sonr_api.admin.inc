<?php

/**
 * @file
 * Administration functionality for the sOnr API.
 */

/**
 * List all saved sOnr API configurations.
 *
 * @return string
 *   The rendered HTML of the list of configurations.
 */
function sonr_api_list() {
  $configs = sonr_api_config_load();

  $rows = array();
  foreach ($configs as $config) {
    $actions = array(
      l(t('Edit'), 'admin/config/services/sonr-api/' . $config->getId() . '/edit'),
//      l(t('Delete'), 'admin/config/services/sonr-api/' . $config->getId() . '/delete'),
    );
    $rows[] = array(
      $config->getTitle(),
      $config->getUrl(),
      '<div class="sonr-led" data-id="' . $config->getId() . '" title="Checking service"></div>',
      implode(' | ', $actions),
    );
  }
  $table = array(
    'header' => array(
      t('Name'),
      t('URL to the sOnr server'),
      t('Available'),
      t('Actions'),
    ),
    'rows' => $rows,
  );

  return theme('table', $table);
}

/**
 * Ajax callback function for checking if a new PoolParty server is available.
 */
function sonr_api_new_available($form, $form_state) {
  $available = '<div id="health_info" class="available">' . t('The server is available.') . '</div>';
  $not_available = '<div id="health_info" class="not-available">' . t('The server is not available or the credentials are incorrect.') . '</div>';

  if (isset($form_state['values']['url']) && valid_url($form_state['values']['url'], TRUE)) {
    // Create a new connection (without saving) with the current form data.
    $connection = SemanticConnector::getConnection('sonr_server');
    $connection->setURL($form_state['values']['url']);
    $connection->setCredentials(array(
      'username' => $form_state['values']['name'],
      'password' => $form_state['values']['pass'],
    ));
    $sonr_api = $connection->getAPI();
    return $sonr_api->available() ? $available : $not_available;
  }

  return $not_available;
}

/**
 * Ajax callback function for checking if a sOnr API is available.
 */
function sonr_api_available($sonr_id) {
  $sonr_api = sonr_api_get_api($sonr_id);
  echo $sonr_api->available() ? 1 : 0;
  exit();
}

/**
 * The form for a sOnr API configuration.
 */
function sonr_api_form($form, &$form_state, $config = NULL) {
  if ($config instanceof SemanticConnectorConnection && $config->getID() > 0) {
    $is_configuration_new = FALSE;
    $form['sonrid'] = array(
      '#type' => 'hidden',
      '#value' => $config->getID(),
    );
  }
  else {
    $is_configuration_new = TRUE;
    $config = sonr_api_config_load(0);
  }

  $form['server_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Server settings'),
  );

  $connections = sonr_api_config_load();
  if (!empty($connections)) {
    $connection_options = array();
    foreach ($connections as $connection) {
      $credentials = $connection->getCredentials();
      $connection_options[implode('|', array($connection->getTitle(), $connection->getURL(), $credentials['username'], $credentials['password']))] = $connection->getTitle();
    }
    $form['server_settings']['load_connection'] = array(
      '#type' => 'select',
      '#title' => t('Load an available sOnr server'),
      '#options' => $connection_options,
      '#empty_option' => '',
      '#default_value' => '',
    );

    $form['server_settings']['load_connection_button'] = array(
      '#type' => 'button',
      '#value' => t('Load'),
      '#attributes' => array(
        'onclick' => '
        var connection_value = (jQuery(\'#edit-load-connection\').val());
        if (connection_value.length > 0) {
          var connection_details = connection_value.split(\'|\');
          jQuery(\'#edit-title\').val(connection_details[0]);
          jQuery(\'#edit-url\').val(connection_details[1]);
          jQuery(\'#edit-name\').val(connection_details[2]);
          jQuery(\'#edit-pass\').val(connection_details[3]);
        }
        return false;',
      ),
    );
  }

  $form['server_settings']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Server title'),
    '#description' => t('A short title for the server below.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $config->getTitle(),
    '#required' => TRUE,
  );

  $form['server_settings']['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' => t('URL, where the sOnr server runs.'),
    '#size' => 35,
    '#maxlength' => 255,
    '#default_value' => $config->getURL(),
    '#required' => TRUE,
  );

  $credentials = $config->getCredentials();
  $form['server_settings']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#description' => t('Name of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 60,
    '#default_value' => $credentials['username'],
  );

  $form['server_settings']['pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#description' => t('Password of a user for the credentials.'),
    '#size' => 35,
    '#maxlength' => 128,
    '#default_value' => $credentials['password'],
  );

  $form['server_settings']['health_check'] = array(
    '#type' => 'button',
    '#value' => t('Health check'),
    '#ajax' => array(
      'callback' => 'sonr_api_new_available',
      'wrapper' => 'health_info',
      'method' => 'replace',
      'effect' => 'slide',
    ),
  );

  if ($is_configuration_new) {
    $form['server_settings']['health_info'] = array(
      '#markup' => '<div id="health_info">' . t('Click to check if the server is available.') . '</div>',
    );
  }
  else {
    $available = '<div id="health_info" class="available">' . t('The server is available.') . '</div>';
    $not_available = '<div id="health_info" class="not-available">' . t('The server is not available or the credentials are incorrect.') . '</div>';

    $sonr_api = $config->getAPI();
    $form['server_settings']['health_info'] = array(
      '#markup' => $sonr_api->available() ? $available : $not_available,
    );
  }

  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  $form['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => 'admin/config/services/sonr-api',
  );

  return $form;
}

/**
 * Validate handler for sonr_api_form.
 */
function sonr_api_form_validate($form, &$form_state) {
  if (!valid_url($form_state['values']['url'], TRUE)) {
    form_set_error('url', t('The field URL must be a valid URL.'));
  }
}

/**
 * Submit handler for sonr_api_form.
 */
function sonr_api_form_submit($form, &$form_state) {
  // Always create a new connection, if URL and type are the same the old one
  // will be used anyway.
  SemanticConnector::createConnection('sonr_server', $form_state['values']['url'], $form_state['values']['title'], array(
    'username' => $form_state['values']['name'],
    'password' => $form_state['values']['pass'],
  ));

  drupal_set_message(t('sOnr configuration %title has been saved.', array('%title' => $form_state['values']['title'])));
  $form_state['redirect'] = 'admin/config/services/sonr-api';
}

/**
 * List deletion form.
 * TODO: Move the deleting of a sOnr conf into the connector
 */
function sonr_api_delete_form($form, &$form_state, $conf) {
  $form_state['conf'] = $conf;
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $conf->title)),
    'admin/config/services/sonr-api',
    t('This action cannot be undone.'),
    t('Delete configuration'));
}

/**
 * Submit handler for sonr_api_delete_form().
 * TODO: Move the deleting of a sOnr conf into the connector
 */
function sonr_api_delete_form_submit($form, &$form_state) {
  $conf = $form_state['conf'];
  sonr_api_conf_delete($conf->sonrid);
  drupal_set_message(t('%title has been deleted.', array('%title' => $conf->title)));
  $form_state['redirect'] = 'admin/config/services/sonr-api';
}
